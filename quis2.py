import shapefile #untuk mengimport modul yang bernama shapefile
w=shapefile.Writer() #deklarasi, dalam kurung bisa dikosongkan boleh diisi sesuai type
w.shapeType #berfungsi menjalankan code diatasnya
 
w.field("kolom1","C") #nama kolom1 dengan tipe data character
w.field("kolom2","C") #nama kolom2 dengan tipe data character
 
w.record("cahya","satu") #isi dari kolom 1 cahya, dan kolom 2 satu
w.record("caya","dua") #isi dari kolom 1 caya, dan kolom 2 satu
w.record("cayax","tiga")
w.record("cayae","empat")
w.record("cayad","lima")
w.record("cayag","enam")


 
 
w.line(parts=[[[1,5],[5,5]]]) 
w.line(parts=[[[4,3],[4,1],[3,3]]])
w.line(parts=[[[5,4],[3,2],[2,2]]])
w.line(parts=[[[2,5],[3,7],[6,3]]])
w.line(parts=[[[8,10],[4,6]]])
w.line(parts=[[[-1,-2],[4,-3],[3,7]]])
w.save("quisgis2") #nama file